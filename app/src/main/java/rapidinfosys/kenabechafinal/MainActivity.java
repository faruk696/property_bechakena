package rapidinfosys.kenabechafinal;


import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {


    private final static int SEND_SMS_PERMISSION_REQUEST_CODE = 111;

    Button btnNext, btnBack, btnSend;
    LinearLayout layoutOne, layoutTwo, layoutSell , layoutSqft ,layoutkatha;
    TextView tvSell, tvBuy, tvFlat, tvPlot, tvQuestion ,tvSqft , tvkatha;
    EditText etName, etemail, etLocation, etSellLocation, etMinPrice, etMaxPrice ,etSqft, etkatha;

    private int flag = 0;
    private String msg;
    private String phoneNumber;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSend = findViewById(R.id.button8);
        layoutOne = findViewById(R.id.layout_one);
        layoutTwo = findViewById(R.id.layout_two);
        layoutSell = findViewById(R.id.layout_sell);
        layoutSqft = findViewById(R.id.layout_sqft);
        layoutkatha = findViewById(R.id.layout_katha);
        tvkatha = findViewById(R.id.textView20);
        tvSell = findViewById(R.id.textView13);
        tvBuy = findViewById(R.id.textView14);
        tvPlot = findViewById(R.id.textView10);
        tvFlat = findViewById(R.id.textView11);
        tvSqft = findViewById(R.id.textView16);
        etSqft = findViewById(R.id.editText16);
        etkatha = findViewById(R.id.editText20);
        tvQuestion = findViewById(R.id.textView19);
        etName = findViewById(R.id.editText);
        etemail = findViewById(R.id.editText2);
        etLocation = findViewById(R.id.editText3);
        etMinPrice = findViewById(R.id.editText4);
        etSellLocation = findViewById(R.id.editText6);
        etMaxPrice = findViewById(R.id.editText9);



        //------------------------------for sending SMS--------------------------------------------
        btnSend.setEnabled(false);

        if (checkPermission(Manifest.permission.SEND_SMS)){
            btnSend.setEnabled(true);
        }else {
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.SEND_SMS},SEND_SMS_PERMISSION_REQUEST_CODE);
        }

        //...........................Here i m writing code for sending SMS BY pressing *PATHAN* Button .......................

//        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etName.getText().toString();
                String mobile = etemail.getText().toString();
                String location = etLocation.getText().toString();
                String sellLocation = etSellLocation.getText().toString();
                String minBudget = etMinPrice.getText().toString();
                String maxBudget = etMaxPrice.getText().toString();
                if(name.matches("")|| mobile.matches("")||location.matches("")||sellLocation.matches("")||minBudget.matches("")||maxBudget.matches("")){
                    Toast.makeText(MainActivity.this,"You need Fill up all the field avobe",Toast.LENGTH_LONG).show();
                }
                else{
                    if(flag==1){
                        msg = "Pro-S"+"\n"+"N: "+name+"\n" +"Phn: "+mobile+ "\n" + "Add: "+location+"\n"+"Prefered Location: " + sellLocation + "\n" + "Budget: "+minBudget+" - "+maxBudget;
                        phoneNumber="008801730012300";


                        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getApplicationContext());
                        View mView = layoutInflaterAndroid.inflate(R.layout.custom_dialog,null,false);
                        final android.support.v7.app.AlertDialog.Builder alertDialogBuilderUserInput = new
                                android.support.v7.app.AlertDialog.Builder(MainActivity.this);
                        alertDialogBuilderUserInput.setView(mView);


                        alertDialogBuilderUserInput
                                .setCancelable(false)
                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        etName.setText(null);
                                        etemail.setText(null);
                                        etLocation.setText(null);
                                        etSellLocation.setText(null);
                                        etMaxPrice.setText(null);
                                        etMinPrice.setText(null);
                                        etSqft.setText(null);
                                        etkatha.setText(null);
                                    }
                                });
                        final android.support.v7.app.AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                        alertDialogAndroid.show();

                    }else if(flag==2){
                        msg = "Pro-B"+"\n"+"N: "+name+"\n" +"Phn: "+mobile+ "\n" + "Add: "+location+"\n"+"Prefered Location: " + sellLocation + "\n" + "Budget: "+minBudget+" - "+maxBudget;
                        phoneNumber="008801730012300";



                        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getApplicationContext());
                        View mView = layoutInflaterAndroid.inflate(R.layout.custom_dialog_bikreta,null,false);
                        final android.support.v7.app.AlertDialog.Builder alertDialogBuilderUserInput = new
                                android.support.v7.app.AlertDialog.Builder(MainActivity.this);
                        alertDialogBuilderUserInput.setView(mView);


                        alertDialogBuilderUserInput
                                .setCancelable(false)
                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        etName.setText(null);
                                        etemail.setText(null);
                                        etLocation.setText(null);
                                        etSellLocation.setText(null);
                                        etMaxPrice.setText(null);
                                        etMinPrice.setText(null);
                                        etSqft.setText(null);
                                        etkatha.setText(null);
                                    }
                                });
                        final android.support.v7.app.AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                        alertDialogAndroid.show();

                    }else{
                        Toast.makeText(MainActivity.this,"Plese don't left empty field",Toast.LENGTH_LONG).show();
                    }
                }
                if (!TextUtils.isEmpty(msg) && !TextUtils.isEmpty(phoneNumber)) {
                    if (checkPermission(Manifest.permission.SEND_SMS)){
                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(phoneNumber,null,msg,null,null);
                    } else {
                        Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }else{
                    etName.setText(null);
                    etemail.setText(null);
                    etLocation.setText(null);
                    etSellLocation.setText(null);
                    etMaxPrice.setText(null);
                    etMinPrice.setText(null);
                    etSqft.setText(null);
                    etkatha.setText(null);
                }

                //-----------------------SETTING Up alaert dialog----------------------------------


            }
        });
        tvSell.setOnClickListener(v -> {
            //ei flag ta ami bosia nisi jate pore if else diye kaje lagate pari
            flag = 1;
            layoutSell.setVisibility(View.VISIBLE);
            tvQuestion.setText("কি বেচতে চান?");
            tvSell.setBackground(getResources().getDrawable(R.drawable.shape_textview));
            tvSell.setTextColor(getResources().getColor(R.color.colorTextWhite));
            tvBuy.setBackground(getResources().getDrawable(R.drawable.shape_textview_white));
            tvBuy.setTextColor(getResources().getColor(R.color.colorText));
        });

        tvBuy.setOnClickListener(v -> {
            flag = 2;
            layoutSell.setVisibility(View.VISIBLE);
            tvQuestion.setText("কি কিনতে চান?");
            tvSell.setBackground(getResources().getDrawable(R.drawable.shape_textview_white));
            tvSell.setTextColor(getResources().getColor(R.color.colorText));
            tvBuy.setBackground(getResources().getDrawable(R.drawable.shape_textview));
            tvBuy.setTextColor(getResources().getColor(R.color.colorWhite));
        });

        tvPlot.setOnClickListener(v -> {
            tvPlot.setBackground(getResources().getDrawable(R.drawable.shape_textview));
            tvSell.setTextColor(getResources().getColor(R.color.colorWhite));
            tvFlat.setBackground(getResources().getDrawable(R.drawable.shape_textview_white));
            tvBuy.setTextColor(getResources().getColor(R.color.colorText));

            layoutSqft.setVisibility(View.GONE);
            layoutkatha.setVisibility(View.VISIBLE);
            tvSqft.setText("কতো কাঠা?");
        });

        tvFlat.setOnClickListener(v -> {
            //edit kora hoise ekhane

            tvFlat.setBackground(getResources().getDrawable(R.drawable.shape_textview));
            tvFlat.setTextColor(getResources().getColor(R.color.colorWhite));
            tvPlot.setBackground(getResources().getDrawable(R.drawable.shape_textview_white));
            tvPlot.setTextColor(getResources().getColor(R.color.colorText));

            layoutkatha.setVisibility(View.GONE);
            layoutSqft.setVisibility(View.VISIBLE);
            tvSqft.setText("কতো স্কয়ার ফিট?");
        });
    }
    //..............FOR SENDING SMS.............................
    private boolean checkPermission(String permission) {
        int checkPermission = ContextCompat.checkSelfPermission(this, permission);
        return checkPermission == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case SEND_SMS_PERMISSION_REQUEST_CODE :
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)){
                    btnSend.setEnabled(true);
                }
                break;
        }
    }
}